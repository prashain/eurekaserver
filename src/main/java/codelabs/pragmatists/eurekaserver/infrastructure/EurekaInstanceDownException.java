package codelabs.pragmatists.eurekaserver.infrastructure;

public class EurekaInstanceDownException  extends RuntimeException{

    private static final long serialVersionUID = 1L;



    public EurekaInstanceDownException(String message) {
        super(message);
    }
    public EurekaInstanceDownException(String message, Throwable e) {
        super(message,e);
    }


}
