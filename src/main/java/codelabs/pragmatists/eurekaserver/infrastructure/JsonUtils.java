package codelabs.pragmatists.eurekaserver.infrastructure;
import java.util.Map;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;


public final class JsonUtils {
	private static final ObjectMapper mapper = new ObjectMapper().configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES,
			false);

	private JsonUtils() {
	}

	/**
	 * Serialize a JavaBeans object into JSON string.
	 * 
	 * @param p_value JavaBeans object
	 * @return JSON string
	 */
	public static String serialize(final Object value) {
		try {
			return mapper.writeValueAsString(value);
		} catch (final JsonProcessingException e) {
			throw new InfrastructureServiceException(e, "{\"context\" : \"Exception while processing\"}");
		}
	}

	public static <T> T deserializeIntoType(final String content, final Class<T> t)  {
		try {
		   return mapper.readValue(content, t);
	   } catch (JsonProcessingException e) {
		   throw new InfrastructureServiceException(e,"{\"context\" : \"Exception while deserialising\"}");
	   }

   }

   public static <T> Map<String,T>  deserializeIntoMap(String content,TypeReference<Map<String, T>> t){
	   try {

			return mapper.readValue(content, t);
	   } catch (JsonProcessingException e) {
		   throw new InfrastructureServiceException(e,"{\"context\" : \"Exception while deserialising inot JsonNode\"}");
	   }
  }
   public static JsonNode  serialiseIntoJsonNode(String content){
	   try {

			return mapper.readTree(content);
	   } catch (JsonProcessingException e) {
		   throw new InfrastructureServiceException(e,"{\"context\" : \"Exception while deserialising\"}");
	   }
  }

	}

