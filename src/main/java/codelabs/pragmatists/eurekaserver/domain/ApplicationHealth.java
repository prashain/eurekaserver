package codelabs.pragmatists.eurekaserver.domain;


/**
 * Domain class encapsulating aggregated health of the applications registered with Eureka Server
 */
 public class ApplicationHealth {

    private  String applicationName;

    private String healthStatus;

    public ApplicationHealth(){

    }

    public ApplicationHealth(String appName, String health){
        this.applicationName = appName;
        this.healthStatus = health;
    }
    
    public String getApplicationName(){
        return this.applicationName;
    }

    public String getApplicationHealth(){
        return this.healthStatus;
    }
    
}
