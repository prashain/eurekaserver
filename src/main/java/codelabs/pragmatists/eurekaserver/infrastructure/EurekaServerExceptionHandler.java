package codelabs.pragmatists.eurekaserver.infrastructure;

import java.time.LocalDateTime;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.NoHandlerFoundException;

@ControllerAdvice
public class EurekaServerExceptionHandler {

    @ExceptionHandler(EurekaServerException.class)
	public ResponseEntity<ErrorModel> applicationSwitchHandler(EurekaServerException ex, WebRequest request) {
		ErrorModel errorDetails = new ErrorModel.ErrorModelBuilder().withErrorMessage(ex.getMessage())
				.withTimeStamp(LocalDateTime.now().toString())
				.withErrorDetails(request.getDescription(false)).build();
		return new ResponseEntity<>(errorDetails, HttpStatus.SERVICE_UNAVAILABLE);
	}

    @ExceptionHandler(EurekaInstanceDownException.class)
	public ResponseEntity<ErrorModel> handleUnHealthyInstanceException(EurekaInstanceDownException ex, WebRequest request) {
		ErrorModel errorDetails = new ErrorModel.ErrorModelBuilder().withErrorMessage(ex.getMessage())
				.withTimeStamp(LocalDateTime.now().toString())
				.withErrorDetails(request.getDescription(false)).build();
		return new ResponseEntity<>(errorDetails, HttpStatus.SERVICE_UNAVAILABLE);
	}

	@ExceptionHandler(InfrastructureServiceException.class)
	public ResponseEntity<ErrorModel> infrastructureExceptionHandler(InfrastructureServiceException ex,
			WebRequest request) {
		ErrorModel errorDetails = new ErrorModel.ErrorModelBuilder().withErrorMessage(ex.getMessage())
				.withTimeStamp(LocalDateTime.now().toString())
				.withErrorDetails(request.getDescription(false)).build();
		return new ResponseEntity<>(errorDetails, HttpStatus.INTERNAL_SERVER_ERROR);
	}

	@ExceptionHandler(NoHandlerFoundException.class)
	public ResponseEntity<ErrorModel> resolveNoHandlerException(NoHandlerFoundException ex, WebRequest request) {
		ErrorModel errorDetails = new ErrorModel.ErrorModelBuilder()
				.withErrorMessage(String.format("{\"context\": \"Could not find %s method for the URL %s\"} ", ex.getHttpMethod(),
						ex.getRequestURL()))
				.withTimeStamp(LocalDateTime.now().toString())
				.withErrorDetails(request.getDescription(false)).build();
		return new ResponseEntity<>(errorDetails, HttpStatus.NOT_FOUND);
	}
    
}
