package codelabs.pragmatists.eurekaserver.infrastructure;


public class EurekaServerException  extends RuntimeException{

    private static final long serialVersionUID = 1L;



    public EurekaServerException(String message) {
        super(message);
    }
    public EurekaServerException(String message, Throwable e) {
        super(message,e);
    }


}
