# Repository to demonstrate ServiceRegistry pattern for healthchecking

## Introduction
While microservices are a nice pattern to achieve business agility and improve resilience while mitigating risks from single point of failures, it opens another challenge of ensuring all partners in the distributed design works together.Service Registry is one of the known patterns, which recommends a central component, which  enables service discovery.This set-up can help us query service-registry for healthcheck of the registered microservices.

### Design
The setup includes a service-registry microservice, which leverages **Eureka** an open source component for enabling registering services. SpringBoot framework provides a management interface (**actuator**), which helps query the underlying microservices for administration tasks. The framework builds atop actuator health and integrates it will Eureka healthcheck.
<br>
<br>
![EurekaServerDesign](src/main/resources/appdesign.png)


## How to run
1.  `mvn clean compile`
2.   `mvn clean spring-boot:run`




