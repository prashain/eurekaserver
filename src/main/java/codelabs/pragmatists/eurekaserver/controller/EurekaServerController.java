package codelabs.pragmatists.eurekaserver.controller;

import codelabs.pragmatists.eurekaserver.domain.ApplicationHealth;
import codelabs.pragmatists.eurekaserver.domain.Healthz;
import codelabs.pragmatists.eurekaserver.domain.Instance;
import codelabs.pragmatists.eurekaserver.infrastructure.EurekaInstanceDownException;
import codelabs.pragmatists.eurekaserver.infrastructure.EurekaServerException;
import reactor.core.publisher.Mono;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;


import java.util.ArrayList;

import java.util.List;
import org.springframework.http.HttpStatus;

import org.springframework.web.reactive.function.client.WebClient;

@RestController
public class EurekaServerController {

    private static final String EUREKA_URL = "http://localhost:8761/eureka/apps";
    /**
     * Queries health status of all app instances x Web
     * 
     * @return
     */

    @Autowired
    WebClient.Builder webclientBuilder;

    @GetMapping("/healthz")
    public List<ApplicationHealth> healthz() throws Exception {

        List<ApplicationHealth> al = new ArrayList<>(getHealthz().getApplications().getApplication().size());
        getHealthz().getApplications().getApplication().forEach(app -> {
            Instance instance = app.getInstance().stream().findFirst()
                    .orElseThrow(() -> new RuntimeException("{\"context\" : \"No instance up\"}"));

            al.add(new ApplicationHealth(app.getName(), instance.getStatus()));
        });

        return al;

    }

    @Cacheable
    private Healthz getHealthz() throws Exception{
        WebClient client = webclient();
        return client.get().uri(EUREKA_URL).retrieve().bodyToMono(Healthz.class)
        .blockOptional()
        .orElseThrow(() -> new Exception("{\"context\" : \"Cannot retreive registered application\"}"));

    
    }
    @GetMapping("/healthz/{appName}")
    public Mono<String> healthzDetails( @PathVariable String appName) throws Exception {

       Healthz healthz = getHealthz();
        // For the component whose status is down, fetch health url and stream it back
        String healthURL  = healthz.
        getApplications().getApplication()
        .stream().filter(criterion -> criterion.getName().equalsIgnoreCase(appName))
        .findFirst().orElseThrow(() -> new EurekaServerException("{\"context\" : \"Cannot find the application with the requested applicationName\"}"))
        .getInstance()
        .stream().filter(criterion -> criterion.getApp().equalsIgnoreCase(appName)).findFirst().
        orElseThrow(() -> new EurekaServerException("{\"context\" : \"Cannot find the application with the requested applicationName\"}"))
        .getHealthCheckUrl();

        WebClient client = webclient();
        return client.get().uri(healthURL)
        .retrieve()
        .onStatus(HttpStatus::isError, clientResponse ->
        clientResponse.bodyToMono(String.class)
            .handle((error, sink) -> 
                sink.error(new EurekaInstanceDownException(error) {
                    
                }
            )
    ))
        .bodyToMono(String.class);
    }

    private WebClient webclient() {
        return webclientBuilder.defaultHeader(HttpHeaders.ACCEPT, MediaType.APPLICATION_JSON_VALUE).build();
    }

}
