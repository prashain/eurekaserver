package codelabs.pragmatists.eurekaserver.infrastructure;




public class InfrastructureServiceException extends RuntimeException {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public InfrastructureServiceException(String message) {
		super(message);
	}

	public InfrastructureServiceException(Exception e) {
	}

	public InfrastructureServiceException(Exception e, String string) {
	}

	

}

