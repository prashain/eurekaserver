package codelabs.pragmatists.eurekaserver.infrastructure;

import com.fasterxml.jackson.databind.JsonNode;



public class ErrorModel {
	private final String timestamp;
	private final JsonNode message;
	private final String details;


	private ErrorModel(ErrorModelBuilder ebuilder) {
		this.timestamp = ebuilder.ebTimestamp;
		this.message = ebuilder.ebMessage;
		this.details = ebuilder.ebDetails;
	}

	public String getTimestamp() {
		return timestamp;
	}

	public JsonNode getMessage() {
		return message;
	}

	public String getDetails() {
		return details;
	}


	public static class ErrorModelBuilder {
		private String ebTimestamp;
		private JsonNode ebMessage;
		private String ebDetails;

		public ErrorModelBuilder withTimeStamp(String timeStamp) {
			ebTimestamp = timeStamp;
			return this;
		}


		public ErrorModelBuilder withErrorMessage(String errorMessage) {
			ebMessage = JsonUtils.serialiseIntoJsonNode(errorMessage);
			return this;
		}

		public ErrorModelBuilder withErrorDetails(String errorDetails) {
			ebDetails = errorDetails;
			return this;
		}

		public ErrorModel build() {
			return new ErrorModel(this);
		}

	}

	@Override
	public String toString() {
		return JsonUtils.serialize(this);
	}
	
}